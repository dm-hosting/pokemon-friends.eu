<?php
namespace App\Components;


class MostActiveCodesTableControl extends BaseCodesTableControl
{
	public function __construct(\App\Models\Codes $codes, int $itemsPerPage = 20)
	{
		parent::__construct($codes, $itemsPerPage);

		$firstDay = new \Nette\Utils\DateTime();
		$firstDay->setDate(date('Y'),date('n'),1);
		$firstDay->setTime(0,0,0);
		$this->setResource(
			$this->getResource()->where(['updated_at >= ?'=>$firstDay->format('Y-m-d H:i:s')])
		);
	}

	public function render()
	{
		parent::render();

		$this->template->setFile(__DIR__.'/templates/BaseCodesTable.latte');
		$this->template->paginator = $this->initPaginator();
		$this->template->codes = $this->getResource()->order('number_of_updates DESC, updated_at DESC');
		$this->template->pagerAnchor = '#most-active-codes';

		$this->template->render();
	}
}