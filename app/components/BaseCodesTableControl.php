<?php
namespace App\Components;

abstract class BaseCodesTableControl extends \Nette\Application\UI\Control
{
	/** @var @persistent */
	public $page = 1;

	const MAX_PAGES = 50;

	/** @var \App\Models\Codes $codes */
	protected $codes;

	/** @var int $itemsPerPage */
	protected $itemsPerPage;

	/** @var \Nette\Database\Table\Selection $resource */
	protected $resource = null;

    protected $countryCode = null;
    protected $vivillonShortcut = null;

    protected $cacheKey = 'all';

	public function __construct(\App\Models\Codes $codes, int $itemsPerPage)
	{
		$this->codes = $codes;
		$this->itemsPerPage = $itemsPerPage;
	}

	protected function createTemplate() : \Nette\Application\UI\Template
	{
		$template = parent::createTemplate();

		$template->getLatte()->addFilter('friendCode','App\Presenters\HomepagePresenter::formatFriendCode');
		$template->getLatte()->addFilter('friendCodeSpaces','App\Presenters\HomepagePresenter::formatFriendCodeSpaces');
		$template->getLatte()->addFilter('formatDateDiff','App\Presenters\HomepagePresenter::formatDateDiff');

		return $template;
	}

	/**
	 * Gets database resource for table in current state
	 *
	 * @return \Nette\Database\Table\Selection
	 */
	public function getResource()
	{
		if ($this->resource === null)
			return $this->codes->findBy([]);
		else
			return $this->resource;
	}

	/**
	 * Sets resource for table
	 *
	 * @param \Nette\Database\Table\Selection $resource
	 */
	public function setResource(\Nette\Database\Table\Selection $resource)
	{
		$this->resource = $resource;
	}

    public function setVivillonShortcut(string $vivillonShortcut)
    {
        $this->countryCode = null;
        $this->vivillonShortcut = $vivillonShortcut;
        $this->cacheKey = 'vivillon_'.$vivillonShortcut;
        $this->setResource(
            $this->getResource()->where(['vivillon_shortcut'=>$vivillonShortcut])
        );
    }

    public function setCountryCode(string $countryCode)
    {
        $this->vivillonShortcut = null;
        $this->countryCode = $countryCode;
        $this->cacheKey = 'country_'.$countryCode;
        $this->setResource(
            $this->getResource()->where('country_code', $countryCode)
        );
    }

	/**
	 * Initializes paginator and sets paging to resource
	 *
	 * @return \Nette\Utils\Paginator
	 */
	public function initPaginator(): \Nette\Utils\Paginator
	{
		$maxItems = self::MAX_PAGES * $this->itemsPerPage;
        $resource = $this->getResource();
        $resCount = $this->codes->getCache()->load('count_'.$this->cacheKey, function (&$dependencies) use ($resource) {
            $dependencies[\Nette\Caching\Cache::EXPIRE] = '5 minutes';
            return $resource->count('id');
        });

		$count = $resCount > $maxItems ? $maxItems : $resCount;

		$paginator = new \Nette\Utils\Paginator();
		$paginator->setItemsPerPage($this->itemsPerPage)
			->setItemCount($count)
			->setPage($this->page);

		$this->setResource(
			$this->getResource()->page($paginator->getPage(),$paginator->getItemsPerPage())
		);

		return $paginator;
	}

	public function render(){
		if ($this->page > self::MAX_PAGES) {
			throw new \Nette\Application\BadRequestException('Page not found',404);
		}
	}

}