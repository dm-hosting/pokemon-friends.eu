<?php
namespace App\Components;

use App\Models\VivillonRegions;
use Nette;
use Tracy\Debugger;

class VivillonPickerControl extends \Nette\Application\UI\Control
{
	/** @var VivillonRegions */
    protected $vivillonRegions;


	public function __construct(VivillonRegions $vivillonRegions)
    {
        $this->vivillonRegions = $vivillonRegions;
    }

    protected function createComponentForm(): Nette\Application\UI\Form
    {
        $form = new Nette\Application\UI\Form();
        $options = $this->vivillonRegions->getKeyValueList();
        $form->addSelect('region','Search by Vivillon region',$options)
            ->setHtmlAttribute('class','form-control vivillon-select')
            ->setPrompt('Select region')
            ->setRequired('Please select Vivillon region');
        $form->addSubmit('submit','Submit');
        /*if (array_key_exists($this->getPresenter()->getAction(),$options)) {
            $form['region']->setDefaultValue($this->getPresenter()->getAction());
        }*/
        return $form;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/VivillonPickerControl.latte');
        $template->vivillonRegions = $this->vivillonRegions->getKeyValueList();
        $template->render();
    }
}