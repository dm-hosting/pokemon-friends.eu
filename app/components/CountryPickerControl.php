<?php
namespace App\Components;

use App\Models\Countries;
use Nette;
use Tracy\Debugger;

class CountryPickerControl extends \Nette\Application\UI\Control
{
	/** @var Countries */
    protected $countries;


	public function __construct(Countries $vivillonRegions)
    {
        $this->countries = $vivillonRegions;
    }

    protected function createComponentForm(): Nette\Application\UI\Form
    {
        $form = new Nette\Application\UI\Form();
        $options = $this->countries->getKeyValueList();
        $form->addSelect('country','Search by country',$options)
            ->setHtmlAttribute('class','select2 country-select')
            ->setPrompt('Select country')
            ->setRequired('Please select country');
        $form->addSubmit('submit','Submit');
        /*if (array_key_exists($this->getPresenter()->getAction(),$options)) {
            $form['region']->setDefaultValue($this->getPresenter()->getAction());
        }*/
        return $form;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/CountryPickerControl.latte');
        $template->vivillonRegions = $this->countries->getKeyValueList();
        $template->render();
    }

    public function renderTcg()
    {
        $template = $this->template;
        $this['form']['country']->setHtmlAttribute('class','select2 tcg-country-select');
        $template->setFile(__DIR__ . '/templates/CountryPickerControl.latte');
        $template->vivillonRegions = $this->countries->getKeyValueList();
        $template->render();
    }
}