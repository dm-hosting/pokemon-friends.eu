<?php
namespace App\Components;

/**
 * Komponenta pro zobrazeni obrazku
 * Pouziti:
 * <img src="{control image:Name '1_30_2.jpg',218,NULL}" width="{control image:X}" height="{control image:Y}" alt="" />
 */
class ImageControl extends \Nette\Application\UI\Control
{
	protected $cache;
	/** @var string $datadir */
	protected $datadir;

	/** @var string $cachedir */
	protected $cachedir;

	/** @var \Nette\Utils\Image */
	protected $image;

	/** @var int $imgmode */
	protected $imgmode;

	/** @var string $emptyimage */
	protected $emptyimage = NULL;


	public function __construct($documentroot='htdocs',$emptyimage=NULL)
	{
		if (isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'])
		{
			$this->datadir = $_SERVER['DOCUMENT_ROOT'].'/storage/';
			$this->cachedir = $_SERVER['DOCUMENT_ROOT'].'/imagecache/';
		}
		else
		{
			$this->datadir = __DIR__.'/../../../'.$documentroot.'/storage/';
			$this->cachedir = __DIR__.'/../../../'.$documentroot.'/imagecache/';
		}
		$this->imgmode = \Nette\Utils\Image::EXACT;
		$this->emptyimage = $emptyimage;
	}


	/**
	 * Vrati imgmode
	 * @return int
	 */
	public function getImgmode()
	{
		return $this->imgmode;
	}


	/**
	 * Nastavi imgmode
	 * @param int $imgmode
	 * @return ImageControl
	 */
	public function setImgmode($imgmode)
	{
		$this->imgmode = $imgmode;
		return $this;
	}


	/**
	 * Nastavi datadir
	 * @param string $datadir
	 * @return ImageControl
	 */
	public function setDatadir($datadir)
	{
		$this->datadir = $datadir;
		return $this;
	}

	/**
	 * Nastavi cachedir
	 * @param string $cachedir
	 * @return ImageControl
	 */
	public function setCachedir($cachedir)
	{
		$this->cachedir = $cachedir;
		return $this;
	}

	/**
	 * Nastavi nazev prazdneho obrazku
	 * @param $emptyimage
	 * @return $this
	 */
	public function setEmptyImage($emptyimage)
	{
		$this->emptyimage = $emptyimage;
		return $this;
	}


	protected function generateEmpty($x,$y)
	{
		if ($this->emptyimage && is_readable($this->datadir.$this->emptyimage)) {
			$cachename = intval($x).'x'.intval($y).'_'.$this->emptyimage;
			return $this->generateImage($this->emptyimage,$cachename,$x,$y,true);
		}
		else {
			$cachename = intval($x).'x'.intval($y).'_'.'empty.jpg';
			$x = ($x?$x:$y);
			$y = ($y?$y:$x);
			$this->image = \Nette\Utils\Image::fromBlank($x,$y,Array('red'=>255,'green'=>255,'blue'=>255));
			file_put_contents($this->cachedir.$cachename, (string)$this->image);
			return $cachename;
		}
	}

	/**
	 * Vygeneruje nebo vrati cache
	 * @param string $filename
	 * @param string $cachename
	 * @param int $x
	 * @param int $y
	 * @param int[] $sizes
	 * @param bool $optimize
	 * @param bool $skipexceptions
	 * @return string
	 */
	public function generateImage($filename,$cachename,$x,$y,$sizes=[],$optimize=true,$skipexceptions=false)
	{
		$this->image = NULL;

		if ($filename && is_readable($this->datadir.$filename))
		{
			if (!is_readable($this->cachedir.$cachename) ||
				(filemtime($this->cachedir.$cachename)<filemtime($this->datadir.$filename)))
			{
				try {
					$this->image = \Nette\Utils\Image::fromFile($this->datadir.$filename);
					$this->image->resize($x,$y,$this->getImgmode());
					$this->image->alphaBlending(false);
					$this->image->saveAlpha(true);
					$this->image->save($this->cachedir.$cachename,100);
				}
				catch (\Nette\Utils\UnknownImageFileException $e) {
					return ($skipexceptions?$this->generateEmpty($x,$y):'');
				}
			}
			else
			{
				try {
					$this->image = \Nette\Utils\Image::fromFile($this->cachedir.$cachename);
				}
				catch (\Nette\Utils\UnknownImageFileException $e) {
					return ($skipexceptions?$this->generateEmpty($x,$y):'');
				}
			}
		}
		else
		{
			if (strlen($filename)==0 || !is_readable($this->cachedir.$cachename))
				return $this->generateEmpty($x,$y);
			else
				$this->image = \Nette\Utils\Image::fromFile($this->cachedir.$cachename);
		}

		/*$factory = new \ImageOptimizer\OptimizerFactory();
		$optimizer = $factory->get();

		//optimized file overwrites original one
		$optimizer->optimize($this->cachedir.$cachename);*/

		return $cachename;
	}


	/**
	 * Vrati nazev obrazku s cache
	 * @param string $filename
	 * @param int $x
	 * @param int $y
	 * @param int|NULL $mode
	 * @return string
	 */
	public function generateName($filename,$x,$y,$mode=NULL)
	{
		if ($mode!==NULL)
			$this->setImgMode($mode);

		if (($x===NULL || $y===NULL) && $this->getImgmode()==\Nette\Utils\Image::EXACT)
			$this->setImgmode(\Nette\Utils\Image::FIT);

		$cachename = intval($x).'x'.intval($y).'_'.$filename;

		try	{
			$cachename = $this->generateImage($filename,$cachename,$x,$y);
		} catch (\Exception $e)	{
			print '';
		}

		return '/imagecache/'.$cachename;
	}


	/**
	 * Vypise nazev obrazku s cache
	 * @param string $filename
	 * @param int $x
	 * @param int $y
	 * @param int|NULL $mode
	 */
	public function renderName($filename,$x,$y,$mode=NULL)
	{
		print $this->generateName($filename,$x,$y,$mode);
	}

	/**
	 * Renders responsive picture tag, defaulting to bootstrap 4 presets
	 *
	 * @param $filename
	 * @param string $alt
	 * @param int[] $sizes
	 * @param null $mode
	 * @param string $imgClass
	 */
	public function renderResponsive($filename,$alt='',$imgClass='img-fluid',$mode=NULL,$sizes=[360=>540,540=>720,720=>960,960=>1140])
	{
		$cached = [];
		if (!empty($sizes)) {
			foreach ($sizes as $minWidth => $size) {
				$cached[$minWidth] = $this->generateName($filename,$size,NULL,$mode);
			}
		}

		$this->template->setFile(__DIR__.'/templates/image.latte');

		$this->template->cached = $cached;
		$this->template->original = $filename;
		$this->template->alt = $alt;
		$this->template->imgClass = $imgClass;

		$this->template->render();
	}

	/**
	 * Vrati sirku obrazku
	 */
	public function renderX()
	{
		if ($this->image)
			print $this->image->getWidth();
	}


	/**
	 * Vrati vysku obrazku
	 */
	public function renderY()
	{
		if ($this->image)
			print $this->image->getHeight();
	}
}