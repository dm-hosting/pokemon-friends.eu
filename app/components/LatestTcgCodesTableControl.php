<?php
namespace App\Components;


class LatestTcgCodesTableControl extends BaseTcgCodesTableControl
{
	public function __construct(\App\Models\TcgCodes $codes, int $itemsPerPage = 20)
	{
		parent::__construct($codes, $itemsPerPage);

        $this->setResource(
			$this->getResource()
		);

	}

	public function render()
	{
		parent::render();

		$this->template->setFile(__DIR__.'/templates/BaseTcgCodesTable.latte');
		$this->template->paginator = $this->initPaginator();
		$this->template->codes = $this->getResource()->order('updated_at DESC');
		$this->template->pagerAnchor = '#latest-codes';

		$this->template->render();
	}
}