<?php
namespace App\Models;

use Nette;

class Codes extends Base
{
	/**
	 * Return count of all rows in table
	 * @return int
	 */
	public function countAll()
	{
		return $this->cache->load('count_all_codes', function (&$dependencies) {
			$dependencies[Nette\Caching\Cache::EXPIRE] = '5 minutes';
			return $this->getTable()->count('id');
		});
	}
}