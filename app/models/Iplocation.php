<?php
namespace App\Models;

use Nette;

class Iplocation extends Base
{
	/**
	 * Gets country data by given ip
	 *
	 * @param string $ipAddress
	 * @return array
	 */
	public function getCountryByIp(string $ipAddress)
	{
        return $this->cache->load('ip_loc_'.$ipAddress, function (&$dependencies) use ($ipAddress) {
            if (preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/m',$ipAddress))
                $longIp = $this->dot2LongIP($ipAddress);
            else
                $longIp = 0;

            return $this->findBy(['ip_from <= ?'=>$longIp,'ip_to >= ?'=>$longIp])->fetch()->toArray();
        });
	}

	/**
	 * Converts dot format of IP address to decimal number
	 *
	 * @param string $ipAddress
	 * @return int
	 */
	public function dot2LongIP (string $ipAddress)
	{
		if ($ipAddress == "") {
			return 0;
		} else {
			$ips = explode(".", "$ipAddress");
			return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
		}
	}
}