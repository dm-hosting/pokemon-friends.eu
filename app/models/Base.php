<?php
namespace App\Models;

use Nette;

abstract class Base
{
	use \Nette\SmartObject;

	/**
	 * @var \Nette\Database\Explorer
	 */
	protected $database;

	/**
	 * @var Nette\Caching\Cache
	 */
	protected $cache;

	public function __construct(\Nette\Database\Explorer $database, Nette\Caching\Storage $cacheStorage)
	{
		$this->database = $database;
		$this->cache = new Nette\Caching\Cache($cacheStorage, 'model');
	}

	/**
	 * Gets table by model's class
	 * @return Nette\Database\Table\Selection
	 */
	protected function getTable()
	{
		$reflect = new \ReflectionClass($this);
		return $this->database->table(Tools::camelCaseToUnderscore($reflect->getShortName()));
	}

	/**
	 * Gets basic data source for datagrid component
	 * @return Nette\Database\Table\Selection
	 */
	public function getSource()
	{
		return $this->getTable();
	}

	/**
	 * Inserts row in a table.
	 * @param  array|\Traversable|Nette\Database\Table\Selection $data array($column => $value)|\Traversable|Selection for INSERT ... SELECT
	 * @return Nette\Database\Table\IRow|int|bool Returns IRow or number of affected rows for Selection or table without primary key
	 */
	public function insert($data)
	{
		return $this->getTable()->insert($data);
	}

	/**
	 * Updates row in a table.
	 * @param int $id
	 * @param  iterable ($column => $value)
	 * @return int number of affected rows
	 */
	public function update($id,$data)
	{
		return $this->getTable()->where(['id'=>$id])->update($data);
	}

	/**
	 * Deletes all rows in result set.
	 * @param $id
	 * @return int number of affected rows
	 */
	public function delete($id)
	{
		return $this->getTable()->where(['id'=>$id])->delete();
	}

	/**
	 * Return results by given condition
	 * @param array $condition
	 * @return Nette\Database\Table\Selection
	 */
	public function findBy(array $condition)
	{
		return $this->getTable()->where($condition);
	}

    public function getCache(): Nette\Caching\Cache
    {
        return $this->cache;
    }
}