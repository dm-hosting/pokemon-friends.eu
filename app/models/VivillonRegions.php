<?php
namespace App\Models;

use Nette;

class VivillonRegions extends Base
{
    public function getShortcutById(int $id)
    {
        return $this->cache->load('vivillon_region_shortcut_'.$id, function (&$dependencies) use ($id) {
            $dependencies[Nette\Caching\Cache::EXPIRE] = '1 day';
            return $this->findBy(['id'=>$id])->fetch()->shortcut;
        });
    }

    public function getNameById(int $id)
    {
        return $this->cache->load('vivillon_region_name_'.$id, function (&$dependencies) use ($id) {
            $dependencies[Nette\Caching\Cache::EXPIRE] = '1 day';
            return $this->findBy(['id'=>$id])->fetch()->name;
        });
    }

    public function getKeyValueList()
    {
        return $this->cache->load('vivillon_region_key_value_list', function (&$dependencies) {
            $dependencies[Nette\Caching\Cache::EXPIRE] = '1 day';
            $pairs = $this->findBy([])->fetchPairs('id','name');
            foreach ($pairs as $pairKey => $pairValue) {
                Nette\Utils\Arrays::renameKey($pairs,$pairKey,Nette\Utils\Strings::webalize($pairValue));
            }
            return $pairs;
        });
    }
}