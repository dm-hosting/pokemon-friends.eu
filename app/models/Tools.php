<?php
namespace App\Models;

use Nette;

class Tools
{
	/**
	 * @param string $input
	 * @return string
	 */
	public static function camelCaseToUnderscore($input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}

	/**
	 * @param int $length
	 * @return string
	 */
	public static function generateToken($length = 48)
	{
		return bin2hex(random_bytes($length/2));
	}

	/**
	 * Replaces {variables} in string
	 * @param string $string
	 * @param array $variables
	 * @return mixed
	 */
	public static function replaceVariables($string, array $variables)
	{
		foreach ($variables as $varName => $varValue) {
			$string = str_replace('{'.$varName.'}',$varValue,$string);
		}
		return $string;
	}
}