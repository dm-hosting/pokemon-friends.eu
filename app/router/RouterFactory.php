<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = new Route('[<locale=en en|cz|de|es|it|fr|pt|sk|ru|pl>/]tcg-country[/<shortcut>][/<country>]', 'TcgCountry:detail');
        $router[] = new Route('[<locale=en en|cz|de|es|it|fr|pt|sk|ru|pl>/]country[/<shortcut>][/<country>]', 'Country:detail');
		$router[] = new Route('[<locale=en en|cz|de|es|it|fr|pt|sk|ru|pl>/]<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}
}
