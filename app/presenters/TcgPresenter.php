<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


final class TcgPresenter extends BasePresenter
{
	/** @var \App\Models\TcgCodes @inject */
	public $codes;

    /** @var \App\Models\IpLocation @inject */
    public $iplocation;

    /** @var \App\Models\VivillonRegions @inject */
    public $vivillonRegions;

	protected function beforeRender(): void
	{
		parent::beforeRender();
		$this->template->addFilter('friendCode', 'App\Presenters\TcgPresenter::formatFriendTcgCode');
		$this->template->addFilter('friendCodeSpaces', 'App\Presenters\TcgPresenter::formatFriendTcgCodeSpaces');
		$this->template->addFilter('formatDateDiff', 'App\Presenters\HomepagePresenter::formatDateDiff');
	}

	public static function formatFriendTcgCode (string $code): string {
		return sprintf('%016d',$code);
	}

	public static function formatFriendTcgCodeSpaces (string $code): string {
		$code = sprintf('%016d',$code);
		return substr($code,0,4).' '.substr($code,4,4).' '.substr($code,8,4).' '.substr($code,12,4);
	}

	/**
	 * @return Form
	 */
	protected function createComponentPostCodeForm()
	{
		$form = new Form;
		$form->setTranslator($this->translator);
		$form->addText('code', 'Post your code')
			->addRule(Form::FILLED,'ui.form.error.empty')
			->addRule(Form::LENGTH,'ui.form.error.length',16)
			->addRule(Form::NUMERIC, 'ui.form.error.numeric');
		$form->addSubmit('ok', 'ui.submit');
		$form->addProtection();
		$form->onSuccess[] = [$this, 'postCodeFormSucceeded'];
		return $form;
	}

	/**
	 * @param Form $form
	 * @param object $values
	 */
	public function postCodeFormSucceeded(Form $form, $values)
	{
        if ($form['ok']->isSubmittedBy()) {
            $code = $this->codes->findBy(['code'=>$values->code])->fetch();
            $ipAddress = getenv('REMOTE_ADDR');
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $ipAddress = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $country = $this->iplocation->getCountryByIp($ipAddress);

            if ($code) {
                $now = new Nette\Utils\DateTime();
                if ($now->modifyClone('-20 hours') > $code->updated_at) {
                    $data = [
                        'updated_at'=>new \DateTime(),
                        'number_of_updates'=>($code->number_of_updates + 1),
                        'country_code' => strtolower($country['country_code']),
                        'iplocation_id' => $country['id'],
                        'vivillon_shortcut' => $country['vivillon_regions_id'] ? $this->vivillonRegions->getShortcutById($country['vivillon_regions_id']) : null
                    ];
                    $result = $code->update($data);
                    if ($result) {
                        $this->flashMessage($this->translator->translate('ui.form.flash.success-update'),'success');
                    } else {
                        $this->flashMessage($this->translator->translate('ui.form.flash.error-update'),'danger');
                    }
                } else {
                    $diff = $now->diff($code->updated_at->modifyClone('+20 hours'));
                    $this->flashMessage($this->translator->translate('ui.form.flash.error-soon',$diff->h,$diff->i),'warning');
                }

            } else {
                $data = [
                    'code' => $values->code,
                    'updated_at'=>new Nette\Utils\DateTime(),
                    'created_at'=>new Nette\Utils\DateTime(),
                    'country_code' => strtolower($country['country_code']),
                    'iplocation_id' => $country['id'],
                    'vivillon_shortcut' => $country['vivillon_regions_id'] ? $this->vivillonRegions->getShortcutById($country['vivillon_regions_id']) : null
                ];
                if ($this->codes->insert($data)) {
                    $this->flashMessage($this->translator->translate('ui.form.flash.success-insert'),'success');
                } else {
                    $this->flashMessage($this->translator->translate('ui.form.flash.error-insert'),'danger');
                }
            }
        } else {
            $this->flashMessage($this->translator->translate('ui.form.flash.general-warning'),'warning');
        }

		$this->redirect('Tcg:');
	}

	/**
	 * @return \App\Components\LatestTcgCodesTableControl
	 */
	protected function createComponentLatestCodesTable()
	{
		return new \App\Components\LatestTcgCodesTableControl($this->codes,20);
	}

	/**
	 * @return \App\Components\MostActiveTcgCodesTableControl
	 */
	protected function createComponentMostActiveCodesTable()
	{
		return new \App\Components\MostActiveTcgCodesTableControl($this->codes,20);
	}

}
