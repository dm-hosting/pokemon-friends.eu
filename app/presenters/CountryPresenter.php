<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


final class CountryPresenter extends BasePresenter
{
    /** @var \App\Models\Codes @inject */
    public $codes;

    /**
     * @return \App\Components\LatestCodesTableControl
     */
    protected function createComponentLatestCodesTable()
    {
        return new \App\Components\LatestCodesTableControl($this->codes,20);
    }

    /**
     * @return \App\Components\MostActiveCodesTableControl
     */
    protected function createComponentMostActiveCodesTable()
    {
        return new \App\Components\MostActiveCodesTableControl($this->codes,20);
    }

    public function renderDetail(string $shortcut, string $country)
    {
        $this->template->countryName = $this->countries->getCountryNameByUrl($shortcut.'/'.$country);
        $this->template->countryShortcut = $shortcut;
        $this['latestCodesTable']->setCountryCode($shortcut);
        $this['mostActiveCodesTable']->setCountryCode($shortcut);
    }

}
