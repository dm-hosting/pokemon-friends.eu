<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


final class GuidePresenter extends BasePresenter
{
	protected function createComponentImage()
	{
		return new \App\Components\ImageControl();
	}
}
