<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


final class VivillonPresenter extends BasePresenter
{
    /** @var \App\Models\VivillonRegions @inject */
    public $vivillonRegions;

    /** @var \App\Models\Codes @inject */
    public $codes;

    /** @var int $regionId */
    public $regionId;

    protected function createComponentImage()
	{
		return new \App\Components\ImageControl();
	}

    /**
     * @return \App\Components\LatestCodesTableControl
     */
    protected function createComponentLatestCodesTable()
    {
        return new \App\Components\LatestCodesTableControl($this->codes,20);
    }

    /**
     * @return \App\Components\MostActiveCodesTableControl
     */
    protected function createComponentMostActiveCodesTable()
    {
        return new \App\Components\MostActiveCodesTableControl($this->codes,20);
    }

    public function renderDetail()
    {
        $regionShortcut = $this->vivillonRegions->getShortcutById($this->regionId);
        $this['latestCodesTable']->setVivillonShortcut($regionShortcut);
        $this['mostActiveCodesTable']->setVivillonShortcut($regionShortcut);
        $this->template->regionId = $this->regionId;
        $this->template->regionShortcut = $regionShortcut;
        $this->template->regionName = $this->vivillonRegions->getNameById($this->regionId);
    }

    public function actionArchipelago()
    {
        $this->regionId = 1;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features bright yellow wings with black spots arranged in a circular pattern. The pattern is reminiscent of the sun or a starburst, and it represents a region with many small islands.';
    }

    public function actionContinental()
    {
        $this->regionId = 2;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has brown wings with white spots and a white band across the center. The wings give off an earthy vibe, and the white band across the center looks like a continent, hence the name.';
    }

    public function actionElegant()
    {
        $this->regionId = 3;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features light purple wings with white spots and a dark purple band across the center. The wings look delicate and graceful, and the purple color gives off a regal vibe.';
    }

    public function actionGarden()
    {
        $this->regionId = 4;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has light green wings with small white spots. The pattern resembles a garden full of small plants or flowers, and the green color represents growth and vitality.';
    }

    public function actionHighPlains()
    {
        $this->regionId = 5;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features brown wings with small white spots. The pattern is simple but elegant, and the brown color represents a vast expanse of land.';
    }

    public function actionIcySnow()
    {
        $this->regionId = 6;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has white wings with small black spots and a black band across the center. The wings look like a snowy landscape, and the black band across the center resembles a mountain range.';
    }

    public function actionJungle()
    {
        $this->regionId = 7;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features green wings with large white spots. The pattern resembles a lush jungle full of foliage and wildlife, and the green color represents growth and vitality.';
    }

    public function actionMarine()
    {
        $this->regionId = 8;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has light blue wings with small white spots. The pattern looks like a calm sea or a clear sky, and the blue color represents tranquility and peace.';
    }

    public function actionMeadow()
    {
        $this->regionId = 9;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features green wings with large, brightly colored spots. The pattern looks like a meadow full of flowers and wildlife, and the bright spots represent the diversity of life found in nature.';
    }

    public function actionModern()
    {
        $this->regionId = 10;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has black wings with small white spots and a brightly colored band across the center. The pattern looks sleek and stylish, and the bright band across the center adds a pop of color.';
    }

    public function actionMonsoon()
    {
        $this->regionId = 11;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features pinkish-purple wings with white spots and a dark purple band across the center. The pattern looks like a tropical storm, and the pinkish-purple color represents the intense energy of the storm.';
    }

    public function actionOcean()
    {
        $this->regionId = 12;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has dark blue wings with small white spots and a light blue band across the center. The pattern looks like a deep ocean, and the dark blue color represents the depth and mystery of the ocean.';
    }

    public function actionPolar()
    {
        $this->regionId = 13;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features light blue wings with small white spots and a white band across the center. The pattern looks like a snowy landscape, and the light blue color represents the coldness and beauty of the Arctic.';
    }

    public function actionRiver()
    {
        $this->regionId = 14;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has light blue wings with small white spots and a pink band across the center. The pattern looks like a flowing river, and the pink band represents the life-giving water that sustains the ecosystem around the river.';
    }

    public function actionSandstorm()
    {
        $this->regionId = 15;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features light brown wings with small white spots and a darker brown band across the center. The pattern looks like a sandstorm, and the brown colors represent the harshness of the desert environment.';
    }

    public function actionSavanna()
    {
        $this->regionId = 16;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has orange wings with small white spots. The pattern looks like a savanna full of grasses and wildlife, and the orange color represents the warmth and vitality of the African sun.';
    }

    public function actionSun()
    {
        $this->regionId = 17;
        $this->setView('detail');
        $this->template->regionText = 'This pattern features bright yellow wings with small black spots and a dark orange band across the center. The pattern looks like a bright sun, and the yellow and orange colors represent the warmth and energy of the sun.';
    }

    public function actionTundra()
    {
        $this->regionId = 18;
        $this->setView('detail');
        $this->template->regionText = 'This pattern has white wings with small black spots and a black band across the center. The pattern looks like a snowy landscape, and the black band across the center represents the mountain ranges that divide the tundra.';
    }
}
