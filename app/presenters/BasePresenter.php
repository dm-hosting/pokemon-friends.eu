<?php

namespace App\Presenters;

use Nette\Utils\Strings;

abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
	/** @persistent $locale */
	public $locale;

	/** @var \Contributte\Translation\Translator @inject */
	public $translator;

    /** @var \App\Models\VivillonRegions @inject */
    public $vivillonRegions;

    /** @var \App\Models\Countries @inject */
    public $countries;

	public function handleChangeLocale(string $locale): void
	{
		$this->redirect('this',['locale'=>$locale]);
	}

	protected function beforeRender()
	{
		$this->template->localesList = $this->translator->getAvailableLocales();
		$this->template->locale = $this->translator->getLocale();
		$this->template->offerLocale = $this->resolveAcceptedLang($this->translator->getAvailableLocales(),$this->translator->getLocale());
	}

	protected function resolveAcceptedLang(array $locales, string $activeLocale) : ?string
	{
		$acceptedLangs = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
		if (isset($acceptedLangs[0])) {
			$accepted = strtolower($acceptedLangs[0]);
			$accepted = $accepted === 'cs' ? 'cz' : $accepted;

			if (strtolower($activeLocale) !== $accepted && in_array($accepted,$locales)) {
				return $accepted;
			}
		}

		return null;
	}

    protected function createComponentVivillonPicker(): \App\Components\VivillonPickerControl
    {
        return new \App\Components\VivillonPickerControl($this->vivillonRegions);
    }

    protected function createComponentCountryPicker(): \App\Components\CountryPickerControl
    {
        return new \App\Components\CountryPickerControl($this->countries);
    }
}
