<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


final class TcgCountryPresenter extends BasePresenter
{
    /** @var \App\Models\TcgCodes @inject */
    public $codes;

    /**
     * @return \App\Components\LatestTcgCodesTableControl
     */
    protected function createComponentLatestCodesTable()
    {
        return new \App\Components\LatestTcgCodesTableControl($this->codes,20);
    }

    /**
     * @return \App\Components\MostActiveTcgCodesTableControl
     */
    protected function createComponentMostActiveCodesTable()
    {
        return new \App\Components\MostActiveTcgCodesTableControl($this->codes,20);
    }

    public function renderDetail(string $shortcut, string $country)
    {
        $this->template->countryName = $this->countries->getCountryNameByUrl($shortcut.'/'.$country);
        $this->template->countryShortcut = $shortcut;
        $this['latestCodesTable']->setCountryCode($shortcut);
        $this['mostActiveCodesTable']->setCountryCode($shortcut);
    }

}
