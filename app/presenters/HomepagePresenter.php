<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


final class HomepagePresenter extends BasePresenter
{
	/** @var \App\Models\Codes @inject */
	public $codes;

	/** @var \App\Models\Iplocation @inject */
	public $iplocation;

	protected function beforeRender(): void
	{
		parent::beforeRender();
		$this->template->addFilter('friendCode', 'App\Presenters\HomepagePresenter::formatFriendCode');
		$this->template->addFilter('friendCodeSpaces', 'App\Presenters\HomepagePresenter::formatFriendCodeSpaces');
		$this->template->addFilter('formatDateDiff', 'App\Presenters\HomepagePresenter::formatDateDiff');
	}

	public static function formatFriendCode (string $code): string {
		return sprintf('%012d',$code);
	}

	public static function formatFriendCodeSpaces (string $code): string {
		$code = sprintf('%012d',$code);
		return substr($code,0,4).' '.substr($code,4,4).' '.substr($code,8,4);
	}

	public static function formatDateDiff (?\DateTime $dateTime) {
		if (is_null($dateTime)) return 'unknown';
		$now = new \DateTime();
		$interval = $now->diff($dateTime);
		if ($interval->invert == 0) return 'just now';
		if ($interval->y == 1) return $interval->format('a year ago');
		if ($interval->y > 1) return $interval->format('%y years ago');
		if ($interval->m == 1) return $interval->format('a month ago');
		if ($interval->m > 1) return $interval->format('%m months ago');
		if ($interval->d == 1) return $interval->format('yesterday');
		if ($interval->d > 1) return $interval->format('%d days ago');
		if ($interval->h == 1) return $interval->format('an hour ago');
		if ($interval->h > 1) return $interval->format('%h hours ago');
		if ($interval->i == 1) return $interval->format('a minute ago');
		if ($interval->i > 1) return $interval->format('%i minutes ago');
		return $interval->format('just now');
	}

	/**
	 * @return Form
	 */
	protected function createComponentPostCodeForm()
	{
		$form = new Form;
		$form->setTranslator($this->translator);
		$form->addText('code', 'Post your code')
			->addRule(Form::FILLED,'ui.form.error.empty')
			->addRule(Form::LENGTH,'ui.form.error.length',12)
			->addRule(Form::NUMERIC, 'ui.form.error.numeric');
		$form->addSubmit('ok', 'ui.submit');
		$form->addProtection();
		$form->onSuccess[] = [$this, 'postCodeFormSucceeded'];
		return $form;
	}

	/**
	 * @param Form $form
	 * @param object $values
	 */
	public function postCodeFormSucceeded(Form $form, $values)
	{
		if ($form['ok']->isSubmittedBy()) {
			$code = $this->codes->findBy(['code'=>$values->code])->fetch();
			$ipAddress = getenv('REMOTE_ADDR');
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $ipAddress = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
			$country = $this->iplocation->getCountryByIp($ipAddress);

			if ($code) {
				$now = new Nette\Utils\DateTime();
				if ($now->modifyClone('-20 hours') > $code->updated_at) {
					$data = [
						'updated_at'=>new \DateTime(),
						'number_of_updates'=>($code->number_of_updates + 1),
						'country_code' => strtolower($country['country_code']),
						'iplocation_id' => $country['id'],
                        'vivillon_shortcut' => $country['vivillon_regions_id'] ? $this->vivillonRegions->getShortcutById($country['vivillon_regions_id']) : null
					];
					$result = $code->update($data);
					if ($result) {
						$this->flashMessage($this->translator->translate('ui.form.flash.success-update'),'success');
					} else {
						$this->flashMessage($this->translator->translate('ui.form.flash.error-update'),'danger');
					}
				} else {
					$diff = $now->diff($code->updated_at->modifyClone('+20 hours'));
					$this->flashMessage($this->translator->translate('ui.form.flash.error-soon',$diff->h,$diff->i),'warning');
				}

			} else {
				$data = [
					'code' => $values->code,
					'updated_at'=>new Nette\Utils\DateTime(),
					'created_at'=>new Nette\Utils\DateTime(),
					'country_code' => strtolower($country['country_code']),
					'iplocation_id' => $country['id'],
                    'vivillon_shortcut' => $country['vivillon_regions_id'] ? $this->vivillonRegions->getShortcutById($country['vivillon_regions_id']) : null
				];
				if ($this->codes->insert($data)) {
					$this->flashMessage($this->translator->translate('ui.form.flash.success-insert'),'success');
				} else {
					$this->flashMessage($this->translator->translate('ui.form.flash.error-insert'),'danger');
				}
			}
		} else {
			$this->flashMessage($this->translator->translate('ui.form.flash.general-warning'),'warning');
		}

		$this->redirect('Homepage:');
	}

	/**
	 * @return \App\Components\LatestCodesTableControl
	 */
	protected function createComponentLatestCodesTable()
	{
		return new \App\Components\LatestCodesTableControl($this->codes,20);
	}

	/**
	 * @return \App\Components\MostActiveCodesTableControl
	 */
	protected function createComponentMostActiveCodesTable()
	{
		return new \App\Components\MostActiveCodesTableControl($this->codes,20);
	}

}
