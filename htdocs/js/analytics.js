//window.dataLayer = window.dataLayer || [];
//currentConsent = localStorage.getItem("consent");
consentWindow = document.getElementById("consent");

function hideConsent() {
    consentWindow.style.display = "none";
}

function showConsent() {
    consentWindow.style.display = "block";
}

//function gtag(){ dataLayer.push(arguments); }

if (currentConsent !== null) {
    hideConsent();
}

let grantButton = document.getElementById("cookie-grant-consent");
grantButton.addEventListener("click", function(e) {
    e.preventDefault();
    localStorage.setItem("consent", "granted");
    function gtag() { dataLayer.push(arguments); }

    gtag('consent', 'update', {
        ad_user_data: 'granted',
        ad_personalization: 'granted',
        ad_storage: 'granted',
        analytics_storage: 'granted'
    });
    hideConsent();
});

let denyButton = document.getElementById("cookie-deny-consent");
denyButton.addEventListener("click", function(e) {
    e.preventDefault();
    localStorage.setItem("consent", "denied");
    function gtag() { dataLayer.push(arguments); }

    gtag('consent', 'update', {
        ad_user_data: 'denied',
        ad_personalization: 'denied',
        ad_storage: 'denied',
        analytics_storage: 'denied'
    });
    hideConsent();
});

// Load Tag Manager script.
/*let gtmScript = document.createElement('script');
gtmScript.async = true;
gtmScript.src = 'https://www.googletagmanager.com/gtm.js?id=G-VTKLW2YCG9';

let firstScript = document.getElementsByTagName('script')[0];
firstScript.parentNode.insertBefore(gtmScript,firstScript);*/