const CACHE_NAME = 'pokemon-friends-cache-v1';
const urlsToCache = [
    '/css/datagrid.min.css',
    '/css/datagrid-spinners.min.css',
    '/js/qrcode.min.js',
    '/js/datagrid.min.js',
    '/js/datagrid-spinners.min.js',
    '/js/datagrid-instant-url-refresh.min.js'
    // Add other assets you want to cache
];

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(cache => {
                return cache.addAll(urlsToCache);
            })
            .then(() => {
                // Send event to Google Analytics
                fetch('https://www.google-analytics.com/collect', {
                    method: 'POST',
                    body: new URLSearchParams({
                        v: '1',
                        tid: 'G-VTKLW2YCG9',
                        cid: '555', // Anonymous Client ID
                        t: 'event',
                        ec: 'Service Worker',
                        ea: 'install',
                        el: 'Service Worker Installed'
                    })
                });
            })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                if (response) {
                    return response;
                }
                return fetch(event.request).then(networkResponse => {
                    if (
                        event.request.url.includes('/images/')
                        || event.request.url.includes('/storage/')
                    ) {
                        return caches.open(CACHE_NAME).then(cache => {
                            cache.put(event.request, networkResponse.clone());
                            return networkResponse;
                        });
                    }
                    return networkResponse;
                });
            })
    );
});