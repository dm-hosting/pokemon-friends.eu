/**
 * Clipboard.js
 * https://gist.github.com/rproenca/64781c6a1329b48a455b645d361a9aa3
 */

window.Clipboard = (function(window, document, navigator) {
	var textArea,
		copy;

	function isOS() {
		return navigator.userAgent.match(/ipad|iphone/i);
	}

	function createTextArea(text) {
		textArea = document.createElement('textArea');
		textArea.value = text;
		document.body.appendChild(textArea);
	}

	function selectText() {
		var range,
			selection;

		if (isOS()) {
			range = document.createRange();
			range.selectNodeContents(textArea);
			selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
			textArea.setSelectionRange(0, 999999);
		} else {
			textArea.select();
		}
	}

	function copyToClipboard() {
		document.execCommand('copy');
		document.body.removeChild(textArea);
	}

	copy = function(text) {
		createTextArea(text);
		selectText();
		copyToClipboard();
	};

	return {
		copy: copy
	};
})(window, document, navigator);

/**
 * global
 */

$(document).on('click','[data-qrcopy]',function(e){
	/*var copyText = document.getElementById(this.dataset.qrcopy);
	copyText.select();
	document.execCommand("copy");*/
	Clipboard.copy(this.dataset.qrcopy);
	var button = e.target;
	button.text = 'Coppied!';
	button.classList.remove('btn-outline-secodary');
	button.classList.add('btn-outline-success');
});

var qrcode = new QRCode("main-qr", {
	text: '',
	width: 180,
	height: 180,
	colorDark : "#000000",
	colorLight : "#ffffff",
	correctLevel : QRCode.CorrectLevel.H
});

qrcode.clear();

$(document).on('click','.show-qrcode',function(e){
	var td = e.target.closest('td');
	var code = td.dataset.qrcode;
	qrcode.makeCode(code);
	document.getElementById('qr-placeholder').remove();
});

$(document).scroll(function(e){
	var viewportOffset = document.getElementById('qr-break').getBoundingClientRect();
	var qr = document.getElementById('main-qr');
	if (viewportOffset.top < 440) {
		qr.style.display = 'none';
	} else {
		qr.style.display = 'block';
	}
});

$('.select2').select2({
	theme: 'bootstrap4',
});

$(document).on('change','.vivillon-select',function(e){
	window.location.href = '/vivillon/'+$(this).val();
});

$(document).on('change','.country-select',function(e){
	window.location.href = '/country/'+$(this).val();
});

$(document).on('change','.tcg-country-select',function(e){
	window.location.href = '/tcg-country/'+$(this).val();
});