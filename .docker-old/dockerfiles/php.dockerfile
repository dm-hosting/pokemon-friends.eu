FROM php:7.4-fpm-alpine
 
WORKDIR /var/www/html

RUN docker-php-ext-install mysqli pdo pdo_mysql
 
RUN addgroup -g 1000 pokemon && adduser -G pokemon -g pokemon -s /bin/sh -D pokemon

USER pokemon

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer self-update
#RUN composer install
#RUN composer update

#RUN apk add --update npm
#RUN npm install

# RUN chown -R pagebuilder:pagebuilder .