FROM composer:latest
 
RUN addgroup -g 1000 pokemon && adduser -G pokemon -g pokemon -s /bin/sh -D pokemon

USER pokemon

WORKDIR /var/www/html
 
ENTRYPOINT [ "composer" ]