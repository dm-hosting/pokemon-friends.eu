# Prerequisities
1. Docker is installed on your machine
2. All commands assume that you are in the directory with *docker-compose.yaml* file !!
# Setup
## Composer update
`docker-compose run --rm composer update`

Note that first time it takes a lot of time to complete because of many dependencies are pulled by installing google/apiclient-services

## NPM install
`docker-compose run --rm npm install`

# Manage
## Build and run
`docker-compose -p pagebuilder up -d --build --force-recreate`

optionaly in attached mode: `docker-compose up --build --force-recreate`

## Start assembled
`docker-compose start`

## Stop assembled
`docker-compose stop`
