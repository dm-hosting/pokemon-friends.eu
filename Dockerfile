FROM dockette/web:php-74
LABEL authors="David Macek"

# Install packages
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    zip \
